# Algoritmos III

## Frikiminutos Python:

* [Transcripción de voz](../../frikiminutos-2022/README.md#whisper).

## Recursión

Actividades:

* Presentación: ¿qué es la recursión?

**Ejercicio:** "Cálculo del factorial"

Realiza un programa que calcule el factorial de un número entero positivo que se le de como argumento. Recuerda que el factorial de un número entero se calcula multiplicando ese número por todos los enteros positivos menores que él. Por ejemplo, el factorial de 6 es `6*5*4*3*2*1`:

```commandline
python3 factorial.py 6
720
```

Para realizarlo, escribe una función que calcule el factorial de un número n llamándose a si misma para calcular el factorial de n-1.

Solución: [factorial.py](factorial.py)

**Referencias:**

* [Matering recursive programming](https://developer.ibm.com/articles/l-recurs/)
