# Ordenación Lista de Enteros mediante selección

Realiza un programa que ordene de menor a mayor una lista de números que se proporcionen como argumentos en la línea de comandos. Por ejemplo:

```commandline
python3 sort.py 11 8 4 6 5
4 5 6 8 11 
```

Para realizar la ordenación, utiliza el método de ordenación por selección:

* Para cada lugar en la lista, empezando por la izquierda:
  * Llamamos "lugar pivote" a este lugar
  * Busca, entre los números que estén más a la derecha del lugar pivote, el lugar en que esté el número más pequeño. Llamamos a este lugar encontrado "lugar del menor"
  * Intercambia el número que estaba en el lugar pivote con el número que estaba en el lugar del menor.

Solución: [sort.py](sort.py)
