#!/usr/bin/env pythoh3

"""Programa para ordenar enteros que se reciben como argumentos.
Algoritmo de ordenación por selección.
"""

import sys

def find_lower(numbers: list, pivot: int) -> int:
    lower: int = pivot
    for pos in range(pivot, len(numbers)):
        if numbers[pos] < numbers[lower]:
            lower = pos
    return lower

def sorted_list(numbers: list) -> list:
    for pivot_pos in range(len(numbers)):
        lower_pos: int = find_lower(numbers, pivot_pos)
        if lower_pos != pivot_pos:
            (numbers[pivot_pos], numbers[lower_pos]) = \
                (numbers[lower_pos], numbers[pivot_pos])
    
    return numbers
    
def main():
    numbers: list = sys.argv[1:]
    numbers: list = [int(number) for number in numbers]
    
    sorted_numbers : list = sorted_list(numbers)
    
    for number in sorted_numbers:
        print(number, end=" ")
    print()

if __name__ == '__main__':
    main()
