# Algoritmos I

## Ordenación

Actividades:

* Presentación: el problema de la ordenación de listas.
* Presentación: ordenación mediante selección
* Presentación: ordenación mediante inserción

Ejercicios:

**Ejercicio:** Ordenar números mediante selección.
  * Ejercicio realizado en clase
  * [Enunciado](ordNumSeleccion/README.md)

**Ejercicio:** Ordenar caracteres mediante selección.
  * Ejercicio realizado en clase
  * [Enunciado](ordCharSeleccion/README.md)

**Ejercicio a entregar:** Ordenar formatos de imágenes por nivel de compresión.
  * **Fecha de entrega:** 2 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/sortformats/
  * [Enunciado](sortformats/README.md).
  * [Solución](sortformats/sortformats.py)

  **Ejercicio:** Ordenar vector mediante método inserción.
  * Ejercicio realizado en clase (realizado en clase)
  * **Fecha:** 2 de noviembre de 2023
  * [Enunciado](sort_insertion/README.md)

**Ejercicio:** Ordenación de palabras mediante inserción.
  * Ejercicio recomendado (realizado en clase)
  * **Fecha:** 2 de noviembre de 2023
  * [Enunciado](ordena_palabras_ins/README.md)

**Ejercicio a entregar:** Ordenar formatos de imágenes (algoritmo por inserción).
  * **Fecha de entrega:** 8 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/sortformats_ins/
  * [Enunciado](sortformats_ins/README.md).

**Ejercicio:** Ordenar números mediante Quick Sort.
  * Ejercicio realizado en clase
  * [Enunciado](ordNumQuickSort/README.md)

**Ejercicio:** Ordenar caracteres de una palabra mediante Shell.
  * Ejercicio realizado en clase. 
  * [Enunciado](ordCharShell/README.md)

**Ejercicio a entregar:** Ordenar formatos de imágenes (algoritmo Shell).
  * **Fecha de entrega:** 15 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/shell-sort/
  * [Enunciado](sortmusic_shell/README.md).

**Referencias:**

* [Sorting algorithms in Python](https://realpython.com/sorting-algorithms-python/)
* [Sorting Algorithms (Visualization)](https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html)
* [Visualgo/sorting](https://visualgo.net/sorting) (pueden verse visualizaciones de muchos otros algoritmos con pseudo-código)
* [Sorting Visualizer](https://www.sortvisualizer.com/insertionsort/) (también pueden verse visualizaciones de muchos otros algoritmos)
* [ShellShort](https://www.geeksforgeeks.org/shellsort/)
