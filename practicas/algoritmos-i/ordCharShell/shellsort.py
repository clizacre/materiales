import sys

def ShellSort(word: list) -> list:
    
    gap = len(word) // 2
    
    while gap > 0:
        for i in range(gap, len(word)):
            val = word[i].lower()
            j = i
            while j >= gap and word[j - gap] > val:
                word[j] = word[j - gap]
                j -= gap
            word[j] = val
        gap //= 2 
    
    return word

def main():
    word: str = sys.argv[1]
    word_chars: list = list(word)

    sorted_str = ShellSort(word_chars)
    
    word = ''.join(sorted_str)
    print(word)

if __name__ == "__main__":
   main()