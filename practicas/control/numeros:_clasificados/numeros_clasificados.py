#!/usr/bin/python3

# Escribe un programa que pida un número entero.
# Si el número está entre 1 y 5 escribirá "número pequeño",
# si está entre 6 y 10 (incluids) escribirá "número mediano",
# y si es mayor que 10 escribirá "número grade".
# Cuando lo haya hecho, volverá a pedir otro número,
# y así sucesivamente. Terminará cuando el usuario escriba un 0.

seguir = True

while seguir:

    numero = input("Introduce un número entero (0 para salir): ")
    numero = int(numero)
    if numero == 0:
        seguir = False
    elif numero < 0:
        # Este caso no es parte del enunciado pero es buena práctica incluirlo
        print("El número debe ser positivo")
    elif numero <= 5:
        print("Número pequeño")
    elif numero <= 10:
        print("Número mediano")
    else:
        print("Número grande")

print("¡Hemos terminado!")