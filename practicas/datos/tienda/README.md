### Compra en tienda

Escribe un programa que simula la compra de un artículo. Para escribirlo, hay que tener en cuenta los siguientes puntos:

* Lista de artículos: la lista de artículos y su precio se pondrán como argumentos en la línea de comandos, siguiendo el formato `<articulo> <precio>`, tantas veces como artículos se quiera tener en la tienda.
* Listar productos: el programa comenzará listando los productos que hay en la tienda (los que se han indicado como argumentos en la línea de comandos).
* Compra de artículo: el programa preguntará qué artículo se quiere comprar, y qué cantidad.
* Compra realizada: el programa terminará indicando qué artículo se ha comprado, qué cantidad, y cuánto ha sido el importe total de la compra.

El programa mantendrá los artículos disponibles, junto con su precio, en un diccionario, variable global, llamada `articulos`. En este diccionario las claves serán los nombres de los artículos (strings) y los valores sus precios (float).

El programa se llamará `tienda.py`, y tendrá la estructura que se presenta en el fichero `plantilla.py` del repositorio plantilla:

* La variable `articulos` se usará para almacenar los artículos (claves, que serán de tipo `string`)y sus precios (valores, que serán de tipo `float`).
* La función `anadir` añadirá un  artículo al diccionario de artículos, junto con su precio.
* La función `mostrar` mostrará el listado de artículos en la tienda.
* La función `pedir_artículo` pedirá el nombre del artículo al usuario, lo leerá de lo que éste escriba, y lo devolverá como valor de vuelta (`return`). Si el valor leído no está en el diccionario `articulos` (no tenemos el artículo que se quiere comprar), volverá a pedirlo, hasta que se proporcione uno que sí esté. 
* La función `pedir_cantidad` pedirá la cantidad del artículo al usuario, la leerá de lo que éste escriba, y la devolverá como valor de vuelta (`return`) como valor de tipo `float`. Si el valor leído no se puede convertir en `float`, volverá a pedirlo, hasta que pueda.
* La función `main` comenzará añadiendo al diccionario `articulos` (usando la función `anadir`) los artículos aportados como argumentos en la línea de comandos, y sus precios. Continuará mostrando los artículos disponibles en la tienda (llamando a `mostrar`). Pedirá al usuario qué artículo quiere comprar, y en qué cantidad (llamando a `pedir_articulo` y `pedir_cantidad`). Terminará mostrando el artículo comprado y su cantidad, y el precio total de la compra.

Como verás en el fichero `plantilla.py` todo el código va en la función `main`, y el "programa principal" (el código que va después de `if __name__ ...`) servirá simplemente para llamar a `main`, que hará todo el trabajo.

Mira los comentarios de las funciones en el fichero `olantilla.py`, que aportan algún detalle más.

Por ejemplo, una ejecución típica será como sigue:

```commandline
$ python3 tienda.py leche 1.0 pan 0.75 patatas 2.20
Lista de artículos en la tienda:
leche: 1.0 euros
pan: 0.75 euros
patatas: 2.2 euros

Artículo: pan
Cantidad: 2

Compra total: 2.0 de pan, a pagar 1.5
```

Si hay errores en los argumentos proporcionados, el programa terminará:

```commandline
$ python3 tienda.py leche 1.0 pan 0.75 patatas 2.20 lechugas 
Error en argumentos: Hay al menos un artículo sin precio: lechugas.
$ python3 tienda.py leche 1.0 lechugas pan 0.75 patatas 2.20
Error en argumentos: pan no es un precio correcto.
$ python3 tienda.py leche
Error en argumentos: Hay al menos un artículo sin precio: leche.
$ python3 tienda.py
Error en argumentos: no se han especificado artículos.
```

Si el usuario introduce (cuando se le pide) un artículo que no está en la lista de artículos, se le vuelve a pedir:

```commandline
a$ python3 tienda.py leche 1.0 pan 0.75
Lista de artículos en la tienda:
leche: 1.0 euros
pan: 0.75 euros

Artículo: patatas
Artículo: chocolate
Artículo: leche
Cantidad: 2

Compra total: 2.0 de leche, a pagar 2.0
```

Si el usuario introduce (cuando se le pide) un precio que no es un número, se le vuelve a pedir:

```commandline
$ python3 tienda.py leche 1.0 pan 0.75
Lista de artículos en la tienda:
leche: 1.0 euros
pan: 0.75 euros

Artículo: pan
Cantidad: mucho
Cantidad: poco
Cantidad: 2

Compra total: 2.0 de pan, a pagar 1.5
```

Para escribir tu programa te recomendamos que copies el contenido de `esquema.py` en `Tienda.py`, y completes luego `tienda.py` hasta que funcione como se especifica en este enunciado.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el mismo que hemos seguido en ejercicios anteriores: bifurca (fork) el repositorio plantilla, creando así el repositorio de entrega. Clónalo, copia el código de `plantilla.py` a  `tienda.py`, y modifica su contenido hasta que el programa funcione como debe, haciendo commits según vayas avanzando. Sube esos commits (push) al repositorio frecuéntemente. La versión que se evaluará será la que haya en el repositorio de entrega 

Al terminar, comprueba en EIF GitLab que el fichero `tienda.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_tienda.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_tienda.py
```
