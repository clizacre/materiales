# Estructura de datos

Actividades:

* Presentación de transparencias del tema "Estructuras de datos"

Ejercicios:

**Ejercicio a entregar:** Lista de la compra

  * **Fecha de entrega:** 30 de octubre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/compra/
  * [Enunciado](compra/README.md).
  * [Solución](compra/compra.py)

**Ejercicio:** Practicando con un diccionario
  * Ejercicio recomendado
  * [Enunciado](diccionario/README.md)


**Ejercicio a entregar:** 

  * **Fecha de entrega:** 6 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/tienda/
  * [Enunciado](tienda/README.md).
  * [Solución](tienda/tienda.py)