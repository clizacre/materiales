# Estructura de datos II

Actividades:

* Presentación de transparencias del tema "Estructuras de datos II"

* Ejercicios:

**Ejercicio:** Juguemos con strings

  * Ejercicio realizado en clase
  * [Enunciado](strings/README.md)

**Ejercicio:** Conversión a float

  * Ejercicio realizado en clase
  * [Enunciado](es_float/README.md)


**Ejercicio a entregar:** Clasificador de cadenas de texto

  * **Fecha de entrega:** 13 de noviembre de 2023, 23:59
  * Repositorio plantilla: https://gitlab.eif.urjc.es/cursoprogram/Plantillas2023/clasificador/
  * [Enunciado](clasificador/README.md)

